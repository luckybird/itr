from django.forms import ModelForm, ModelChoiceField
from .models import Act, ActBook, ObjectExplotation, ActBookJob, ActJob
from django.contrib.auth.models import User
from django import forms
from django.contrib.admin import widgets
from django.forms.widgets import DateInput
import re

# class ActBookForm(ModelForm):
#     act = forms.ModelChoiceField(queryset=ActBook.objects.all(), widget = forms.Select(attrs={'class': 'form-control'}), label='Выберите шаблон акта', label_suffix=':')

#     class Meta:
#         model = ActBook
#         fields = [

#         ]


class ActForm(ModelForm):
#    notificationfile = forms.FileField(label='Выберите ченджлог', label_suffix=" : ",
#                                 required=True, disabled=False, allow_empty_file=True,
#                                 max_length=500,
#
#                       error_messages={'required': "This field is required."})
    #act_number =  forms.ModelCharField(attrs={'class': 'form-control'}),
    #act_number = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control','size':20}))
    act_book = forms.ModelChoiceField(queryset=ActBook.objects.all(), widget = forms.Select(attrs={'class': 'form-control'}), label='Выберите шаблон акта', label_suffix=':')
    object_explotation = forms.ModelChoiceField(queryset=ObjectExplotation.objects.all(), widget = forms.Select(attrs={'class': 'form-control'}), label='Выберите объект эксплуатации', label_suffix=':')
    technic = forms.ModelChoiceField(queryset=User.objects.all(), widget = forms.Select(attrs={'class': 'form-control'}), label='Выберите сотрудника выполнящего осмотр', label_suffix=':')
#    create_at =  forms.DateField(widget=widgets.AdminDateWidget, label='Дата акта', label_suffix=':'),

    field_order = ['act_book', 'act_number', 'create_at', 'object_explotation','technic' ]

    class Meta:
        model = Act
        exclude = []
        widgets = {
            #'name': forms.TextInput(attrs={'class': 'form-control'}),
            'act_number': widgets.AdminDateWidget,
            'create_at':  DateInput(attrs={'type': 'date'}),


        }
        labels = {
        }

    # def __init__(self, *args, **kwargs):
    #     super(Act, self).__init__(*args, **kwargs)
    #     self.fields['create_at'].widget.attrs['class'] = 'datepicker'


# class ActJobForm(ModelForm):
#     act = forms.ModelChoiceField(queryset=Act.objects.all(), widget = forms.Select(attrs={'class': 'form-control'}), label='Выберите шаблон акта', label_suffix=':')
#     act_book_job = forms.ModelChoiceField(queryset=ActBookJob.objects.all(), widget = forms.Select(attrs={'class': 'form-control'}), label='Поле шаблона', label_suffix=':')
#     act_result = forms.ModelChoiceField(choices=RESULT, widget = forms.Select(attrs={'class': 'form-control'}), label='Выберите шаблон акта', label_suffix=':')

#     class Meta:
#         model = ActJob
#         exclude = []
#         widgets = {
#             #'name': forms.TextInput(attrs={'class': 'form-control'}),
#             'description': forms.TextInput(attrs={'class': 'form-control'}),
#             'fix': forms.TextInput(attrs={'class': 'form-control'}),

#         }
#         labels = {
#         }
