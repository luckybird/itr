from django.db import models
#additional
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User

# Create your models here.


class Owner(models.Model):
    name = models.CharField(max_length=150,  help_text="Названиe владельца", verbose_name="Название владельца")
    contats =  models.TextField(max_length=500,  help_text="Контактныке данные", verbose_name="Контактныке данные")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Владелец объекта'
        verbose_name_plural = 'Владелецы объектов'
        ordering = ['name']


class Type(models.Model):
    name = models.CharField(max_length=150,  help_text="Тип объекта", verbose_name="Тип объекта")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Тип объекта'
        verbose_name_plural = 'Типы объектов'
        ordering = ['name']


class Service(models.Model):
    name = models.CharField(max_length=150,  help_text="Вид обслуживания", verbose_name="Вид обслуживания")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Вид обслуживания'
        verbose_name_plural = 'Виды обслуживания'
        ordering = ['name']


class ObjectExplotation(models.Model):
    accounting_number = models.CharField(max_length=150, help_text="Учетный номер объекта", verbose_name="Учетный номер объекта" )
    registration_number = models.CharField(max_length=150, null=True, blank=True, help_text="Регистрационный номер объекта", verbose_name="Регистрационный номер объекта")
    license_number = models.CharField(max_length=150, null=True, blank=True, help_text="Номер лицензии", verbose_name="Номер лицензии")
    name = models.CharField(max_length=150, null=True, blank=True, help_text="Названиe обекта", verbose_name="Названиe обекта")
    type =  models.ForeignKey(Type, on_delete=models.SET_NULL, null=True, blank=True, default=None, verbose_name="Тип объкта", related_name='objects')
    owner = models.ForeignKey(Owner, on_delete=models.SET_NULL, null=True, blank=True, default=None, help_text="Собственник опасного объекта", verbose_name="Собственник опасного объекта", related_name='objects')
    address =  models.TextField(max_length=500,  help_text="Адрес объекта", verbose_name="Адрес объекта")
    technician = models.ForeignKey(User, on_delete=models.CASCADE, help_text="Обслуживающий техник", verbose_name="Обслуживающий техник")
    service_type = models.ForeignKey(Service, on_delete=models.SET_NULL, null=True, blank=True, default=None, help_text="Тип обслуживания объекта", verbose_name="Тип обслуживания объекта")
    act_rediness = models.CharField(max_length=150, null=True, blank=True, help_text="Номер акта готовности", verbose_name="Номер акта готовности")
    act_rediness_scan = models.FileField(upload_to='scans/', null=True, blank=True, help_text="Скан акта готовности", verbose_name="Скан акта готовности")
    industrial_safety = models.CharField(max_length=150, null=True, blank=True, help_text="Экспертиза промышленнрой безопасности", verbose_name="Экспертиза промышленнрой безопасности")
    industrial_safety_scan = models.FileField(upload_to='scans/', null=True, blank=True, help_text="Скан экспертизы промышленнрой безопасности", verbose_name="Скан экспертизы промышленнрой безопасности")
    act_examination = models.CharField(max_length=150, null=True, blank=True, help_text="Акт первичного технического освидетельствования", verbose_name="Акт первичного технического освидетельствования")
    act_examination_scan = models.FileField(upload_to='scans/', null=True, blank=True, help_text="Скан акта первичного технического освидетельствования", verbose_name="Скан акта первичного технического освидетельствования")
    insurance = models.CharField(max_length=150, null=True, blank=True, help_text="Договор страхования объекта", verbose_name="Договор страхования объекта")
    insurance_scan = models.FileField(upload_to='scans/', null=True, blank=True, help_text="Скан договора страхования объекта", verbose_name="Скан договора страхования объекта")
    insurance = models.CharField(max_length=150, null=True, blank=True, help_text="Договор аварийно-спасательного формирования", verbose_name="Договор аварийно-спасательного формирования")
    insurance_scan = models.FileField(upload_to='scans/', null=True, blank=True, help_text="Скан договора аварийно-спасательного формирования", verbose_name="Скан договора аварийно-спасательного формирования")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Объект'
        verbose_name_plural = 'Объекты'
        ordering = ['name']


class Act(models.Model):
    class Meta:
        verbose_name = 'Акт'
        verbose_name_plural = 'Акты'
        ordering = ['create_at']

    act_number = models.CharField(max_length=20, unique = True, null=True, blank=True,  verbose_name="Номер акта эксплуатации")
    create_at = models.DateField(null=True, blank=True,  verbose_name="Дата акта")
    act_book = models.ForeignKey("ActBook", on_delete=models.SET_NULL, null=True, blank=True, default=None, verbose_name="Тип акта")
    object_explotation = models.ForeignKey("ObjectExplotation", on_delete=models.SET_NULL, null=True, blank=True, default=None, verbose_name="Объект эксплуатации", related_name="acts")
    technic = models.ForeignKey(User, on_delete=models.CASCADE, help_text="Обслуживающий техник", verbose_name="Обслуживающий техник")

    def __str__(self):
        return self.act_number

    def get_count_success_job(self):
        return self.jobs.filter(act_result__in=('NONEREGULAR', 'ACCIDENT', 'INCIDENT' )).count() # noqa


class ActJob(models.Model):
    RESULT = (
        ('REGULAR', 'штатное  обслуживание'),
        ('NONEREGULAR', 'нештатное  обслуживание'),
        ('ACCIDENT', 'авария'),
        ('INCIDENT', 'инцидент'),
    )

    act = models.ForeignKey("Act", on_delete=models.SET_NULL, null=True, blank=True, default=None, verbose_name="Акт", related_name="jobs")
    act_book_job = models.ForeignKey("ActBookJob", on_delete=models.SET_NULL, null=True, blank=True, default=None, verbose_name="Пункт акта")
    act_result = models.CharField(max_length=16,  default=None, choices=RESULT,  verbose_name="Результат")
    description = models.TextField(max_length=500, null=True, blank=True,  help_text="Описание проблемы", verbose_name="Описание проблемы")
    fix = models.TextField(max_length=500, null=True, blank=True, help_text="Проделанные работы", verbose_name="Проделанные работы")

    def __str__(self):
        return str(self.act_book_job.id)

    def clean(self) -> None:
        if not self.act.act_book.act_book_job.filter(pk=self.act_book_job.pk):
            raise ValidationError("выбранные пункт акта не пренадлежит шаблону актов")
        return super().clean()


class ActBook(models.Model):
    class Meta:
        verbose_name = 'Справочник актов'
        verbose_name_plural = 'Справочники актов'
        ordering = ['name']

    name = models.CharField(max_length=150, null=True, blank=True, help_text="Название акта", verbose_name="Название акта")
    #act =  models.ForeignKey(Act, on_delete=models.SET_NULL, null=True, blank=True, default=None, help_text="Акт", verbose_name="Акт")
    act_book_job = models.ManyToManyField("ActBookJob",  null=True, blank=True, default=None, verbose_name="Шаблон акта")
    def __str__(self):
        return self.name


class ActBookJob(models.Model):
    class Meta:
        verbose_name = 'Справочник пунктов акта'
        verbose_name_plural = 'Справочники пунктов акта'
        ordering = ['name']

    name =  models.CharField(max_length=300, null=True, blank=True, help_text="Пункт акта", verbose_name="Пункт акта")

    def __str__(self):
        return self.name


class CompositionObject(models.Model):
    object = models.ForeignKey(ObjectExplotation, on_delete=models.SET_NULL, null=True, blank=True, default=None, verbose_name="Тип объкта", related_name='compositions')
    name = models.CharField(max_length=150, null=True, blank=True, help_text="Названиe состава обьекта", verbose_name="Названиe состава обьекта")
    pipe_lenght = models.DecimalField(max_digits=4, decimal_places=2, null=True, blank=True, help_text="Длинна трубы (метров)", verbose_name="Длинна трубы (метров)")
    pipe_diameter = models.IntegerField(null=True, blank=True, help_text="Диаметр трубы (миллиметры)", verbose_name="Диаметр трубы (миллиметры)")
    pipe_temperature = models.DecimalField(max_digits=4, decimal_places=2, null=True, blank=True, help_text="Рассчетная температура трубы (градусы цельсия)", verbose_name="Рассчетная температура трубы (градусы цельсия)")
    date = models.DateField(null=True, blank=True, help_text="Дата ввода в эксплуатацию", verbose_name="Дата ввода в эксплуатацию")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Компоненты объекта'
        verbose_name_plural = 'Компоненты объектов'
        ordering = ['name']
