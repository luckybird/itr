from django.contrib import admin

# Register your models here.
from django.forms import TextInput, Textarea
from django.db import models

from .models import ObjectExplotation, Type, Owner, Service, CompositionObject, Act, ActJob, ActBook, ActBookJob
from rangefilter.filters import DateRangeFilter, DateTimeRangeFilter, NumericRangeFilter


from django.urls import resolve
from django.contrib.admin import DateFieldListFilter
from django.db.models import Count
from django.db.models import Q

# Register your models here.
admin.site.register(Type)
admin.site.register(Owner)
admin.site.register(Service)
admin.site.register(ActJob)
admin.site.register(ActBookJob)
#admin.site.register(ActType)
#admin.site.register(Object)


class ActJobInline(admin.TabularInline):
    model = ActJob
    extra = 0
    classes = ()

    # def get_form(self, request, obj=None, **kwargs):
    #     import pdb; pdb.set_trace()
    #     form = super(ActJobInline, self).get_form(request, obj, **kwargs)
    #     form.base_fields['act_book_job'].queryset = self.act.act_book.act_book_job.all()
    #     return form

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
     #
        field = super(ActJobInline, self).formfield_for_foreignkey(db_field, request, **kwargs)


        if db_field.name == 'act_book_job':

            resolved = resolve(request.path_info)
            if resolved.kwargs.get('object_id'):
                act = self.parent_model.objects.get(pk=resolved.kwargs['object_id'])
                field.queryset = act.act_book.act_book_job.all()

        return field


class CompositionObjectInline(admin.TabularInline):
    model = CompositionObject
    extra = 0
    classes = ('collapse',)


class ActBookJobInline(admin.TabularInline):
    model = ActBookJob
    extra = 0
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size':'150'})},
    }


class EventStartDateListFilter(admin.SimpleListFilter):
    title = "Результаты работы"
    parameter_name = "count_not_success_job"

    def lookups(self, request, model_admin):
        return (
            ("success", "Успешные"),
            ("not_success", "Не успешные"),
        )

    def queryset(self, request, queryset):
        value = self.value()
        if value == "success":
            return queryset.filter(count_not_success_job=0)
        elif value == "not_success":
            return queryset.filter(count_not_success_job__gt=0)

        return queryset


@admin.register(Act)
class ActAdmin(admin.ModelAdmin):
    search_fields = ['act_number']
    list_display = ('act_number', 'create_at', 'count_not_success_job', )
    ist_filter = (('create_at', DateRangeFilter), ('create_at' ), )
    list_filter = [('create_at', DateRangeFilter), ('create_at' ), EventStartDateListFilter]

    inlines = [ActJobInline]

    def get_rangefilter_created_at_default(self, request):
        return (datetime.date.today, datetime.date.today)

    def get_rangefilter_created_at_title(self, request, field_path):
        return 'custom title'

    def get_queryset(self, request):
        return Act.objects.annotate(count_not_success_job=Count('jobs', filter=Q(jobs__act_result__in=('NONEREGULAR', 'ACCIDENT', 'INCIDENT'))))

    def count_not_success_job(self, obj):
        return obj.count_not_success_job

    count_not_success_job.admin_order_field = 'count_not_success_job'
    count_not_success_job.short_description = 'Кол-во не успешных работ'

    def get_count_success_job(self, obj):
        return obj.count_success_job > 0

    # def render_change_form(self, request, context, *args, **kwargs):
    #     import pdb; pdb.set_trace()
    #     context['adminform'].form.fields['act_book_job'].queryset = self.act.act_book.act_book_job.all()
    #     return super(ActJobInline, self).render_change_form(request, context, *args, **kwargs)


@admin.register(ObjectExplotation)
class ObjectExplotationAdmin(admin.ModelAdmin):
    search_fields = ['accounting_number','name']
    inlines = [CompositionObjectInline]


@admin.register(ActBook)
class ActBookAdmin(admin.ModelAdmin):
    search_fields = ['accounting_number','name']
    #inlines = [ActBookJobInline]


@admin.register(CompositionObject)
class IncidentAdmin(admin.ModelAdmin):
    list_display = ('object', 'name', 'pipe_lenght', 'pipe_diameter','pipe_temperature','date')
    list_filter = ['object', 'name', 'pipe_lenght', 'pipe_diameter','pipe_temperature','date']
    search_fields = ['object','name']
    ordering = ['object']


#@admin.register(Job)
#class JobAdmin(admin.ModelAdmin):
#    list_filter = ['name']
#    formfield_overrides = {
#        models.CharField: {'widget': TextInput(attrs={'size':'150'})},
#    }
