from django.shortcuts import render, redirect
from urllib.request import Request
from django.shortcuts import render, get_object_or_404
from django.views.generic.base import View
from django.contrib.auth.mixins import LoginRequiredMixin
import logging

from .models import ObjectExplotation, ActBookJob, ActJob

from .forms import ActForm
# Create your views here.


class IndexView(LoginRequiredMixin, View):
    def get(self, request):
        return render(request, 'pipe/index.html', {'nbar': 'index'})


class About(LoginRequiredMixin,View):
    def get(self, request):
        return render(request, "pipe/about.html", {'nbar': 'about'})


class AсtCreate(LoginRequiredMixin,View):
    def get(self, request):
        form = ActForm(initial={'act': 0})
        return render(request, "pipe/act_create.html",
                      {"form": form,
                       'nbar': 'act_create',
                       'result_list': ActJob.RESULT
                       })

    def post(self, request):
        """
        сохранение акта и работ по акту
        """

        form = ActForm(request.POST)
        if form.is_valid():
            act = form.save()

            reqest_dict_data = dict(request.POST)

            for i, val in enumerate(reqest_dict_data['result']):
                act_job_data = {
                    'act_id': act.id,
                    'act_book_job_id': reqest_dict_data['act_book_job_id'][i],
                    'act_result': reqest_dict_data['result'][i],
                    'description': reqest_dict_data['description'][i],
                    'fix': reqest_dict_data['fix'][i]
                }

                act_job = ActJob(**act_job_data)
                act_job.save()
            return redirect('/')
        else:
            print('----------------------', form.errors)


class AсtJob(LoginRequiredMixin,View):
    def get(self, request, actbookid=None):
        act_book_jobs = ActBookJob.objects.filter(actbook__id=actbookid)

        return render(request, "pipe/act_job.html", {'act_book_jobs': act_book_jobs , 'result_list': ActJob.RESULT} )
