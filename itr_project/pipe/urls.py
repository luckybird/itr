from django.urls import path, include

from django.conf import settings
from django.conf.urls.static import static

from django.views.i18n import JavaScriptCatalog

from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index_url'),
    path("about/", views.About.as_view(), name='about_url'),
    path("act/create/", views.AсtCreate.as_view(), name='act_create'),
    path("act/job/<int:actbookid>/", views.AсtJob.as_view(), name='act_job'),



] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
