# Generated by Django 4.1.1 on 2022-10-07 14:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('pipe', '0003_remove_actbookjob_actbook_actbook_actbookjob'),
    ]

    operations = [
        migrations.CreateModel(
            name='ActJob',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('act_result', models.CharField(choices=[('REGULAR', 'штатное  обслуживание'), ('NONEREGULAR', 'нештатное  обслуживание'), ('ACCIDENT', 'авария'), ('INCIDENT', 'инцидент')], default=None, max_length=16, verbose_name='Результат')),
                ('description', models.TextField(help_text='Описание проблемы', max_length=500, verbose_name='Описание проблемы')),
                ('fix', models.TextField(help_text='Проделанные работы', max_length=500, verbose_name='Проделанные работы')),
            ],
        ),
        migrations.RemoveField(
            model_name='act',
            name='act_type',
        ),
        migrations.AddField(
            model_name='act',
            name='act_book',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='pipe.actbook', verbose_name='Тип акта'),
        ),
        migrations.AlterField(
            model_name='act',
            name='create_at',
            field=models.DateField(blank=True, help_text='Дата ввода в эксплуатацию', null=True, verbose_name='Дата ввода в эксплуатацию'),
        ),
        migrations.DeleteModel(
            name='ActResult',
        ),
        migrations.AddField(
            model_name='actjob',
            name='act',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='pipe.act', verbose_name='Акт'),
        ),
        migrations.AddField(
            model_name='actjob',
            name='act_book_job',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='pipe.actbookjob', verbose_name='Пункт акта'),
        ),
    ]
